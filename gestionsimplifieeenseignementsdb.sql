-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : mer. 01 mai 2024 à 22:10
-- Version du serveur : 10.4.32-MariaDB
-- Version de PHP : 8.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `gestionsimplifieeenseignementsdb`
--

-- --------------------------------------------------------

--
-- Structure de la table `appartenance`
--

CREATE TABLE `appartenance` (
  `Id_UE` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL,
  `Nombre_Inscrits` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `appartenance`
--

INSERT INTO `appartenance` (`Id_UE`, `id_semester`, `Nombre_Inscrits`) VALUES
(1, 1, 50),
(1, 3, 40),
(2, 2, 30),
(2, 4, 35);

-- --------------------------------------------------------

--
-- Structure de la table `composition`
--

CREATE TABLE `composition` (
  `Id_Diplome` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `composition`
--

INSERT INTO `composition` (`Id_Diplome`, `id_semester`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4);

-- --------------------------------------------------------

--
-- Structure de la table `departement`
--

CREATE TABLE `departement` (
  `ID_Departement` int(11) NOT NULL,
  `Nom` varchar(50) NOT NULL,
  `Id_enseignant_responsable` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `departement`
--

INSERT INTO `departement` (`ID_Departement`, `Nom`, `Id_enseignant_responsable`) VALUES
(1, 'Informatique', 1),
(2, 'Mathematiques', 2),
(3, 'Physique', 3);

-- --------------------------------------------------------

--
-- Structure de la table `diplome`
--

CREATE TABLE `diplome` (
  `Id_Diplome` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `cout_horaire` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `diplome`
--

INSERT INTO `diplome` (`Id_Diplome`, `nom`, `cout_horaire`) VALUES
(1, 'Licence Informatique', 150),
(2, 'Master Mathematiques', 200),
(3, 'Licence Mathematiques', 120),
(4, 'Licence Informatique', 220);

-- --------------------------------------------------------

--
-- Structure de la table `enseignant`
--

CREATE TABLE `enseignant` (
  `Id_enseignant` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `adresse` varchar(50) NOT NULL,
  `tel` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `statut` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `enseignant`
--

INSERT INTO `enseignant` (`Id_enseignant`, `nom`, `prenom`, `adresse`, `tel`, `email`, `statut`) VALUES
(1, 'Dupont', 'Jean', '123 Rue de la République', '0123456789', 'jean.dupont@example.com', 'Professeur'),
(2, 'Durand', 'Marie', '456 Avenue des Champs-Élysées', '9876543210', 'marie.durand@example.com', 'Maitre de conferences'),
(3, 'Martin', 'Alice', '789 Rue de la Liberté', '0123987654', 'alice.martin@example.com', 'Professeur'),
(4, 'Bernard', 'Lucas', '321 Avenue de la Paix', '0987654321', 'lucas.bernard@example.com', 'Maitre de conferences'),
(5, 'Lefebvre', 'Claire', '987 Boulevard de l\'Indépendance', '0123222111', 'claire.lefebvre@example.com', 'Professeur'),
(6, 'Moreau', 'Julien', '654 Rue de la Victoire', '0988776655', 'julien.moreau@example.com', 'Maitre de conferences');

-- --------------------------------------------------------

--
-- Structure de la table `intervention`
--

CREATE TABLE `intervention` (
  `Id_enseignant` int(11) NOT NULL,
  `Id_UE` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `intervention`
--

INSERT INTO `intervention` (`Id_enseignant`, `Id_UE`) VALUES
(1, 1),
(2, 2),
(3, 1),
(4, 2);

-- --------------------------------------------------------

--
-- Structure de la table `semester`
--

CREATE TABLE `semester` (
  `id_semester` int(11) NOT NULL,
  `numero_Semester` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `semester`
--

INSERT INTO `semester` (`id_semester`, `numero_Semester`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4);

-- --------------------------------------------------------

--
-- Structure de la table `ue`
--

CREATE TABLE `ue` (
  `Id_UE` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `heures_cours` int(11) NOT NULL,
  `heures_td` int(11) NOT NULL,
  `heures_tp` int(11) DEFAULT NULL,
  `nbr_groupe_td` int(11) DEFAULT NULL,
  `nbr_groupe_tp` int(11) DEFAULT NULL,
  `coefficient_cours` decimal(15,2) DEFAULT NULL,
  `coefficient_td` decimal(15,2) DEFAULT NULL,
  `coefficient_tp` decimal(15,2) NOT NULL,
  `Id_enseignant_responsable` int(11) DEFAULT NULL,
  `ID_Departement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `ue`
--

INSERT INTO `ue` (`Id_UE`, `nom`, `heures_cours`, `heures_td`, `heures_tp`, `nbr_groupe_td`, `nbr_groupe_tp`, `coefficient_cours`, `coefficient_td`, `coefficient_tp`, `Id_enseignant_responsable`, `ID_Departement`) VALUES
(1, 'Programmation', 30, 20, 10, 2, 3, 1.50, 1.00, 0.67, 1, 1),
(2, 'Algebre', 25, 15, 10, 1, 2, 1.50, 1.00, 0.67, 2, 2),
(3, 'Analyse', 20, 30, 15, 1, 2, 1.50, 1.00, 0.67, 3, 2),
(4, 'Systemes d\'exploitation', 25, 20, 10, 2, 1, 1.50, 1.00, 0.67, 4, 1);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `appartenance`
--
ALTER TABLE `appartenance`
  ADD PRIMARY KEY (`Id_UE`,`id_semester`),
  ADD KEY `id_semester` (`id_semester`);

--
-- Index pour la table `composition`
--
ALTER TABLE `composition`
  ADD PRIMARY KEY (`Id_Diplome`,`id_semester`),
  ADD KEY `id_semester` (`id_semester`);

--
-- Index pour la table `departement`
--
ALTER TABLE `departement`
  ADD PRIMARY KEY (`ID_Departement`),
  ADD KEY `Id_enseignant_responsable` (`Id_enseignant_responsable`);

--
-- Index pour la table `diplome`
--
ALTER TABLE `diplome`
  ADD PRIMARY KEY (`Id_Diplome`);

--
-- Index pour la table `enseignant`
--
ALTER TABLE `enseignant`
  ADD PRIMARY KEY (`Id_enseignant`);

--
-- Index pour la table `intervention`
--
ALTER TABLE `intervention`
  ADD PRIMARY KEY (`Id_enseignant`,`Id_UE`),
  ADD KEY `Id_UE` (`Id_UE`);

--
-- Index pour la table `semester`
--
ALTER TABLE `semester`
  ADD PRIMARY KEY (`id_semester`);

--
-- Index pour la table `ue`
--
ALTER TABLE `ue`
  ADD PRIMARY KEY (`Id_UE`),
  ADD KEY `Id_enseignant_responsable` (`Id_enseignant_responsable`),
  ADD KEY `ID_Departement` (`ID_Departement`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `departement`
--
ALTER TABLE `departement`
  MODIFY `ID_Departement` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `diplome`
--
ALTER TABLE `diplome`
  MODIFY `Id_Diplome` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `enseignant`
--
ALTER TABLE `enseignant`
  MODIFY `Id_enseignant` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `semester`
--
ALTER TABLE `semester`
  MODIFY `id_semester` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `ue`
--
ALTER TABLE `ue`
  MODIFY `Id_UE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `appartenance`
--
ALTER TABLE `appartenance`
  ADD CONSTRAINT `appartenance_ibfk_1` FOREIGN KEY (`Id_UE`) REFERENCES `ue` (`Id_UE`),
  ADD CONSTRAINT `appartenance_ibfk_2` FOREIGN KEY (`id_semester`) REFERENCES `semester` (`id_semester`);

--
-- Contraintes pour la table `composition`
--
ALTER TABLE `composition`
  ADD CONSTRAINT `composition_ibfk_1` FOREIGN KEY (`Id_Diplome`) REFERENCES `diplome` (`Id_Diplome`),
  ADD CONSTRAINT `composition_ibfk_2` FOREIGN KEY (`id_semester`) REFERENCES `semester` (`id_semester`);

--
-- Contraintes pour la table `departement`
--
ALTER TABLE `departement`
  ADD CONSTRAINT `departement_ibfk_1` FOREIGN KEY (`Id_enseignant_responsable`) REFERENCES `enseignant` (`Id_enseignant`);

--
-- Contraintes pour la table `intervention`
--
ALTER TABLE `intervention`
  ADD CONSTRAINT `intervention_ibfk_1` FOREIGN KEY (`Id_enseignant`) REFERENCES `enseignant` (`Id_enseignant`),
  ADD CONSTRAINT `intervention_ibfk_2` FOREIGN KEY (`Id_UE`) REFERENCES `ue` (`Id_UE`);

--
-- Contraintes pour la table `ue`
--
ALTER TABLE `ue`
  ADD CONSTRAINT `ue_ibfk_1` FOREIGN KEY (`Id_enseignant_responsable`) REFERENCES `enseignant` (`Id_enseignant`),
  ADD CONSTRAINT `ue_ibfk_2` FOREIGN KEY (`ID_Departement`) REFERENCES `departement` (`ID_Departement`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
