-- Déclencheur pour limiter le service d’un enseignant :


DELIMITER //
-- Création d'un déclencheur qui s'active avant l'insertion dans la table 'intervention'
CREATE TRIGGER LimiterServiceAvantInsert
BEFORE INSERT ON intervention FOR EACH ROW
BEGIN
  DECLARE totalETD INT; -- Déclaration d'une variable pour stocker le total des ETD
  -- Calcul du total des ETD pour l'enseignant concerné par la nouvelle insertion
  SELECT SUM(192) INTO totalETD
  FROM intervention
  JOIN enseignant ON intervention.Id_enseignant = enseignant.Id_enseignant
  WHERE NEW.Id_enseignant = enseignant.Id_enseignant
    AND enseignant.statut IN ('Maitre de conferences', 'Professeur');
  -- Si le total des ETD dépasse 384, le déclencheur empêche l'insertion
  IF totalETD >= 384 THEN
    SIGNAL SQLSTATE '45000'
    SET MESSAGE_TEXT = 'Un enseignant ne peut pas dépasser 2 services.';
  END IF;
END;
//
DELIMITER ;


--- Procédure stockée pour calculer la charge horaire d’un département :


DELIMITER //
-- Création d'une procédure stockée pour calculer la charge horaire totale d'un département
CREATE PROCEDURE CalculerChargeHoraire(IN IdDepartement INT)
BEGIN
  -- Sélection et somme des heures de cours, TD et TP pour le département spécifié
  SELECT SUM(heures_cours + heures_td + heures_tp) AS ChargeHoraireTotale
  FROM ue
  WHERE ID_Departement = IdDepartement;
END;
//
DELIMITER ;

---Procédure stockée pour calculer le taux d’encadrement d’un département :

DELIMITER //
-- Création d'une procédure stockée pour calculer le taux d'encadrement d'un département
CREATE PROCEDURE CalculerTauxEncadrement(IN IdDepartement INT)
BEGIN
  -- Calcul du taux d'encadrement en divisant le nombre total d'inscrits par le nombre d'enseignants
  SELECT (SUM(Nombre_Inscrits) / COUNT(DISTINCT Id_enseignant)) AS TauxEncadrement
  FROM appartenance
  JOIN ue ON appartenance.Id_UE = ue.Id_UE
  WHERE ue.ID_Departement = IdDepartement;
END;
//
DELIMITER ;
