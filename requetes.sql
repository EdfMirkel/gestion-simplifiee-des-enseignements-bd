--- Requette pour l'enseignants intervenant à la fois en licence d’Informatique et en licence de Mathématiques:

SELECT E.ID_Enseignant, E.Nom, E.Prénom
FROM Enseignant E
JOIN UE U ON E.ID_Enseignant = U.ID_Enseignant
JOIN Semestre S ON U.ID_Semestre = S.ID_Semestre
JOIN Diplôme D ON S.ID_Diplome = D.ID_Diplome
WHERE D.Nom = 'Licence Informatique'
  AND EXISTS (
    SELECT 1
    FROM UE U2
    JOIN Semestre S2 ON U2.ID_Semestre = S2.ID_Semestre
    JOIN Diplôme D2 ON S2.ID_Diplome = D2.ID_Diplome
    WHERE D2.Nom = 'Licence Mathématiques'
      AND U2.ID_Enseignant = E.ID_Enseignant
  );

---  Requette pour l'enseignants ne faisant aucune intervention pour un module géré par le département d’informatique:


SELECT E.ID_Enseignant, E.Nom, E.Prénom
FROM Enseignant E
WHERE NOT EXISTS (
  SELECT 1
  FROM UE U
  JOIN Semestre S ON U.ID_Semestre = S.ID_Semestre
  JOIN Diplôme D ON S.ID_Diplome = D.ID_Diplome
  WHERE D.Nom = 'Licence Informatique'
    AND U.ID_Enseignant = E.ID_Enseignant
);

--- Charge en heures ETD de chaque enseignant (avec 0 pour les enseignants n’ayant aucune heure inscrite dans la base):

SELECT E.ID_Enseignant, E.Nom, E.Prénom,
  COALESCE(SUM(A.Heures_Cours * 1.5 + A.Heures_TD + A.Heures_TP * 2/3), 0) AS Charge_Horaire_ETD
FROM Enseignant E
LEFT JOIN Affectation A ON E.ID_Enseignant = A.ID_Enseignant
GROUP BY E.ID_Enseignant, E.Nom, E.Prénom;

--- Pour obtenir tous les enseignants qui ont le plus grand nombre d’heures ETD

SELECT e.Id_enseignant, e.nom, e.prenom
FROM Enseignant e
JOIN Intervention i ON e.Id_enseignant = i.Id_enseignant
JOIN UE u ON i.Id_UE = u.Id_UE
GROUP BY e.Id_enseignant
HAVING SUM(u.heures_cours * u.coefficient_cours + u.heures_td * u.coefficient_td + u.heures_tp * u.coefficient_tp) = (
  SELECT MAX(total_heures)
  FROM (
    SELECT SUM(u.heures_cours * u.coefficient_cours + u.heures_td * u.coefficient_td + u.heures_tp * u.coefficient_tp) AS total_heures
    FROM Enseignant e
    JOIN Intervention i ON e.Id_enseignant = i.Id_enseignant
    JOIN UE u ON i.Id_UE = u.Id_UE
    GROUP BY e.Id_enseignant
  ) AS subquery
);


--- Coût horaire de chaque semestre de diplôme présents dans la Base de Données et coût horaire total d’un diplôme:


SELECT S.ID_Semestre, S.Numéro, D.Nom AS Diplôme,
  SUM(U.Heures_Cours * 1.5 + U.Heures_TD + U.Heures_TP * 2/3) AS Coût_Horaire_Semestre
FROM Semestre S
JOIN Diplôme D ON S.ID_Diplome = D.ID_Diplome
JOIN UE U ON S.ID_Semestre = U.ID_Semestre
GROUP BY S.ID_Semestre, S.Numéro, D.Nom
ORDER BY S.ID_Semestre;

SELECT D.Nom AS Diplôme, SUM(Coût_Horaire_Semestre) AS Coût_Horaire_Total
FROM (
  SELECT S.ID_Semestre, S.Numéro, D.Nom AS Diplôme,
    SUM(U.Heures_Cours * 1.5 + U.Heures_TD + U.Heures_TP * 2/3) AS Coût_Horaire_Semestre
  FROM Semestre S
  JOIN Diplôme D ON S.ID_Diplome = D.ID_Diplome
  JOIN UE U ON S.ID_Semestre = U.ID_Semestre
  GROUP BY S.ID_Semestre, S.Numéro, D.Nom
) AS Coûts_Semestres
GROUP BY D.Nom;